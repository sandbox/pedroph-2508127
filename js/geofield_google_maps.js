/* @file
 * Javascript for Goole Map widget of geofield.
 */

(function ($) {
  Drupal.behaviors.geolocationGooglemaps = {
    attach: function (context, settings) {

      var field_base = '#edit-' + $('.field-base input').val().replace(/_/g, '-');
      var address = '';
      $(field_base + ' input').each(function () {
        address += this.value + '+';
      });

      var geocoder;
      var map;
      var marker;

      var widgetId = settings.geofieldGoogleMaps.defaults[0].id;
      var lat = settings.geofieldGoogleMaps.defaults[0].lat;
      var lng = settings.geofieldGoogleMaps.defaults[0].lng;

      function initialize() {
        geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(40.452708625193516, -3.67297686874997);
        var mapOptions = {
          zoom: 5,
          center: latlng
        };
        map = new google.maps.Map(document.getElementById('map-' + widgetId), mapOptions);
        if(lat == '' || lng  == ''){
          codeAddress();
        } else {
          var latlng = new google.maps.LatLng(lat, lng);
          map.setCenter(latlng);
          marker = new google.maps.Marker({
            map: map,
            draggable: true,
            position: latlng
          });
          google.maps.event.addListener(marker, 'dragend', function(me) {
            $('#geolocation-lat-' + widgetId + ' input').attr('value', me.latLng.lat());
            $('#geolocation-lat-item-' + widgetId + ' .geolocation-lat-item-value').html(me.latLng.lat());
            $('#geolocation-lng-' + widgetId + ' input').attr('value', me.latLng.lng());
            $('#geolocation-lng-item-' + widgetId + ' .geolocation-lat-item-value').html(me.latLng.lng());
          });
        }
      }

      function codeAddress() {
        var address = '';
        $(field_base + ' input').each(function () {
          address += this.value + '+';
        });
        geocoder.geocode( { 'address': address}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            map.setZoom(16);
            var lat = results[0].geometry.location.A;
            var lng = results[0].geometry.location.F;
            $('#geolocation-lat-' + widgetId + ' input').attr('value', lat);
            $('#geolocation-lat-item-' + widgetId + ' .geolocation-lat-item-value').html(lat);
            $('#geolocation-lng-' + widgetId + ' input').attr('value', lng);
            $('#geolocation-lng-item-' + widgetId + ' .geolocation-lat-item-value').html(lng);
            if (marker) {
              //if marker already was created change positon
              marker.setPosition(results[0].geometry.location);
            }
            else {
              marker = new google.maps.Marker({
                map: map,
                draggable: true,
                position: results[0].geometry.location
              });
              google.maps.event.addListener(marker, 'dragend', function(me) {
                $('#geolocation-lat-' + widgetId + ' input').attr('value', me.latLng.lat());
                $('#geolocation-lat-item-' + widgetId + ' .geolocation-lat-item-value').html(me.latLng.lat());
                $('#geolocation-lng-' + widgetId + ' input').attr('value', me.latLng.lng());
                $('#geolocation-lng-item-' + widgetId + ' .geolocation-lat-item-value').html(me.latLng.lng());
              });
            }
          }
        });
      }

      $('.geolocation-address-geocode').click(function() {
        codeAddress();
      });

      google.maps.event.addDomListener(window, 'load', initialize);

    }
  }

})(jQuery);
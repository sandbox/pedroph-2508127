<?php

/**
 * Implements hook_field_widget_info().
 */
function geofield_google_maps_field_widget_info() {
  $widgets = array();

  $widgets['geofield_google_maps'] = array(
    'label' => t('Google Maps'),
    'field types' => array('geofield'),
  );
  return $widgets;
}

/**
 * Implements hook_field_widget_settings_form().
 */
function geofield_google_maps_field_widget_settings_form($field, $instance) {
  return geocoder_field_widget_settings_form($field, $instance);
}


/**
 * Implements hook_field_widget_form().
 */
function geofield_google_maps_field_widget_form(&$form, &$form_state, $field, $instance,
                                                $langcode, $items, $delta, $element) {

  $id = $instance['id'] . '-' . $delta;

  $lat_value = isset($items[$delta]['lat']) ? $items[$delta]['lat'] : NULL;

  if ($field['type'] == 'geofield') {
    $lng_value = isset($items[$delta]['lon']) ? $items[$delta]['lon'] : NULL;
  }
  else {
    $lng_value = isset($items[$delta]['lng']) ? $items[$delta]['lng'] : NULL;
  }
  $element += array(
    '#delta' => $delta,
  );

  if($instance['widget']['type'] == 'geofield_google_maps') {

    $element['field_base'] = array(
      '#type' => 'hidden',
      '#prefix' => '<div id="field-base-' . $id . '" class="field-base">',
      '#suffix' => '</div>',
      '#default_value' => $instance['widget']['settings']['geocoder_field'],
    );

    $element['geocode'] = array(
      '#prefix' => '<span id="geolocation-address-geocode-' . $id . '" class="geolocation-address-geocode button clearfix">',
      '#suffix' => '</span>',
      '#markup' => t('Get point in the map'),
    );

    $element['googlemap'] = array(
      '#prefix' => '<div id="map-' . $id . '" class="gmap" gmap style="width:100%;height:400px;">',
      '#suffix' => '</div>',
    );
    // Presentational item.
    $element['latitem'] = array(
      '#type' => 'item',
      '#title' => t('Latitude:'),
      '#prefix' => '<div id="geolocation-lat-item-' . $id . '" class="geolocation-lat-item">',
      '#suffix' => '</div>',
      '#markup' => '<span class="geolocation-lat-item-value">' . $lat_value . '</span>',
      '#required' => $instance['required'],
    );
    $element['lat'] = array(
      '#type' => 'hidden',
      '#prefix' => '<div id="geolocation-lat-' . $id . '" class="geolocation-lat">',
      '#suffix' => '</div>',
      '#default_value' => $lat_value,
    );
    // Presentational item.
    $element['lngitem'] = array(
      '#type' => 'item',
      '#title' => t('Longitude:'),
      '#prefix' => '<div id="geolocation-lng-item-' . $id . '" class="geolocation-lng-item">',
      '#suffix' => '</div>',
      '#markup' => '<span class="geolocation-lat-item-value">' . $lng_value . '</span>',
      '#required' => $instance['required'],
    );
    $element['lng'] = array(
      '#type' => 'hidden',
      '#prefix' => '<div id="geolocation-lng-' . $id . '" class="geolocation-lng">',
      '#suffix' => '</div>',
      '#default_value' => $lng_value,
    );

    $element['#attached']['js'][] = array(
      'data' => drupal_get_path('module', 'geofield_google_maps') . '/js/geofield_google_maps.js',
      'type' => 'file',
      'scope' => 'footer',
    );
    geofield_google_maps_attch_google_js($element);

    $map_defaults_lat = ($lat_value) ? $lat_value : '';
    $map_defaults_lng = ($lng_value) ? $lng_value : '';
    $map_defaults = array(
      array(
        'id' => $id,
        'lat' => $map_defaults_lat,
        'lng' => $map_defaults_lng,
      ),
    );
    $data = array(
      'defaults' => $map_defaults,
      'settings' => $instance['widget']['settings'],
    );
    $element['#attached']['js'][] = array(
      'data' => array('geofieldGoogleMaps' => $data),
      'type' => 'setting',
    );
    $element['#element_validate'] = array('geofield_google_maps_field_widget_validate');
    $element['#element_validate'][] = 'geofield_google_maps_field_widget_set_value';
  }
  return $element;
}

function geofield_google_maps_attch_google_js(&$element) {
  $js_added_already = &drupal_static(__FUNCTION__, FALSE);
  if (!$js_added_already) {
    $element['#attached']['js'][] = array(
      'data' => '//maps.google.com/maps/api/js?sensor=false',
      'type' => 'external',
    );
    $element['#attached']['js'][] = array(
      'data' => '//www.google.com/jsapi',
      'type' => 'external',
    );
    $js_added_already = TRUE;
  }
}

function geofield_google_maps_field_widget_validate($element, &$form_state, $form) {
  if ($element['#required']) {
    if (!$element['lat']['#value'] || !$element['lng']['#value']) {
      form_error($element, t('!name field is required.', array('!name' => $element['#title'])));
    }
  }
}

function geofield_google_maps_field_widget_set_value($element, &$form_state, $form) {
  $values = &drupal_array_get_nested_value($form_state['values'], $element['#parents']);
  $values['lon'] = $values['lng'];
  $values = geofield_compute_values($values);
}